import re
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

ONLY_SIMBOLS = re.compile('[.-/--]')

def on_insert_text(editable, new_text, new_text_length, position):
    '''called when text is inserted on an entry'''
    if ONLY_SIMBOLS.match(new_text) is None:
        editable.stop_emission('insert-text')

entry = Gtk.Entry()
entry.connect('insert-text', on_insert_text)
window = Gtk.Window()
window.set_title('only simbols')
window.add(entry)
window.connect('delete-event', Gtk.main_quit)
window.show_all()

Gtk.main()